#
# Copyright (C) 2017 Karl Linden <karl.j.linden@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import os

from waflib import TaskGen

@TaskGen.feature('egypt')
@TaskGen.before('process_source')
def egypt(self):
    self.meths.remove('process_source')

    if not self.bld.env['ENABLE_EGYPT']:
        return

    bldnode = self.bld.bldnode

    source = []
    for x in self.to_list(self.source):
        source.append(x)

    target = bldnode.make_node(self.to_list(self.target)[0])
    basename = '.'.join(target.path_from(bldnode).split('.')[:-1])

    if len(source) > 1:
        cfile = self.bld.bldnode.find_or_declare(basename + '.c')
        self.bld(
            source = source,
            target = cfile,
            rule   = 'cat ${SRC} > ${TGT}'
        )
    else:
        cfile = source[0]

    expand = self.bld.bldnode.make_node(basename + '.expand')
    tg = self.bld(
        features = 'c',
        source = cfile,
    )
    if hasattr(self, 'includes'):
        tg.includes = self.includes

def options(opt):
    opt.load('autooptions')

    egypt = opt.add_auto_option(
            'egypt',
            default=False,
            conf_dest='ENABLE_EGYPT',
            help='Generate callgraphs using egypt and graphviz')
    egypt.find_program('dot')
    egypt.find_program('egypt')

def configure(conf):
    conf.load('autooptions')

def build(bld):
    TaskGen.declare_chain(
        name    ='dot',
        rule    ='${EGYPT} ${SRC} > ${TGT}',
        ext_in  ='.expand',
        ext_out ='.dot'
    )
    TaskGen.declare_chain(
        name    ='svg',
        rule    ='${DOT} -Tsvg -o ${TGT} ${SRC}',
        ext_in  ='.dot',
        ext_out ='.svg'
    )

    # This hook must be called after propagate_uselib_vars, because that
    # method can append essential CFLAGS such as -fPIC, which is not
    # propagated if this method is called before.
    @TaskGen.feature('c')
    @TaskGen.after('process_source')
    @TaskGen.after('propagate_uselib_vars')
    def egypt_c_hook(self):
        if not self.bld.env['ENABLE_EGYPT']:
            return

        for task in self.tasks:
            cfile = task.inputs[0]
            cfile_name = os.path.basename(cfile.abspath())
            if cfile_name.endswith('.c'):
                expand_name = cfile_name.replace('.c', '.expand')
                expand = cfile.parent.find_or_declare(expand_name)
                task.env.append_unique(
                    'CFLAGS',
                    '-fdump-rtl-expand=%s' % expand.path_from(self.bld.bldnode)
                )
                task.set_outputs([expand])
                self.bld(source=expand)

